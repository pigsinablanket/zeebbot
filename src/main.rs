extern crate irc;
extern crate pest;
#[macro_use]
extern crate pest_derive;

use std::env;
use std::thread;
use std::time;

use irc::client::prelude::*;

use pest::Parser;

#[derive(Parser)]
#[grammar = "sentence.pest"]
struct SentenceParser;

fn main() {
    let cfgfile = env::args().nth(1).expect("Please give a config file");
    let server = IrcClient::new(cfgfile).expect("Failed to connect to server");
    server.identify().expect("Failed to identify");
    println!("Identified");
    server.for_each_incoming(|message| {
        if message.source_nickname().is_none() {
            return; //Pain
        }
        let requestor_nick = message.source_nickname().unwrap().to_owned();
        match message.command {
            Command::PRIVMSG(channel, message) => {
                if let Ok(parsed_str) = SentenceParser::parse(Rule::phrase, &message) {
                    //println!("{:?}",&parsed_str.clone());
                    let parsed_str = parsed_str.clone().next().unwrap().into_inner();
                    //println!("Ok");
                    for parsed in parsed_str {
                        let nick = extract_nick(&parsed, &requestor_nick);
                        //println!("{:?}",&parsed.clone());
                        //let parsed = parsed.into_inner().next().unwrap();
                        //println!("Parsed");
                        //println!("{:?}",parsed.as_rule());
                        match parsed.as_rule() {
                            Rule::list => server.send_privmsg(&channel, "zb list").unwrap(),
                            Rule::source => server.send_privmsg(&channel, "https://gitlab.com/waylon531/zeebbot").unwrap(),
                            Rule::help => server.send_privmsg(&channel,
                            "Zello! My name is Zeebbot. I am native to Zeebonia, but I zave come to Zamerica to learn your beautiful languaz. Zeebonia is my native languaz, zo I would be very zappy to zelp you to tranzlate Zeebonian to Zenglish.").unwrap(),
                            Rule::add => {
                                let number = extract_number(&parsed);
                                server.send_privmsg(&channel, &format!("zb add {} as {}",number,&nick)).unwrap();
                            },
                            Rule::sub => {
                                let number = extract_number(&parsed);
                                server.send_privmsg(&channel, &format!("zb add -{} as {}",number,&nick)).unwrap();
                            },
                            Rule::buy => {
                                let item = extract_item(&parsed);
                                server.send_privmsg(&channel, &format!("zb buy {} as {}",item,&nick)).unwrap();
                            },
                            Rule::repeat_buy => {
                                //The item gets treated as a 0, so we divide by 10
                                //to counteract it
                                let number = (extract_number(&parsed)/10.0) as usize;
                                let item = extract_item_inner(&mut parsed.clone().into_inner().skip(1+number/10).next().unwrap());
                                for _ in 0 .. number {
                                    server.send_privmsg(&channel, &format!("zb buy {} as {}",item,&nick)).unwrap();
                                    thread::sleep(time::Duration::from_secs(1));
                                }
                            },
							e @ _ => {println!("{:?}",e)}
                        }
                    }
                }

            }
            _ => ()
        }
    }).unwrap()

}
fn extract_number<'a>(parsed: &pest::iterators::Pair<'a,Rule>) -> f64 {
    let mut digits = String::new();
    for digit in parsed.clone().into_inner() {
        println!("{:?}",&digit.clone());
        digits.push( match digit.as_rule() {
            Rule::zero => '0',
            Rule::one => '1',
            Rule::two => '2',
            Rule::three => '3',
            Rule::four => '4',
            Rule::five => '5',
            Rule::six => '6',
            Rule::seven => '7',
            Rule::eight => '8',
            Rule::nine => '9',
            Rule::decimal => '.',
            _ => '0'
        });
    }
    match digits.parse() {
        Ok(num) => num,
        Err(_) => 0.0
    }
}
fn extract_item<'a>(parsed: &pest::iterators::Pair<'a,Rule>) -> String {
    let item = extract_item_inner(&mut parsed.clone().into_inner().next().unwrap());//.next().unwrap();
    //match item.as_rule() {
    //    Rule::chimi => "chimi",
    //    Rule::burrito => "burrito",
    //    Rule::can => "can",
    //    Rule::bake => "bake",
    //    Rule::chips => "chips",
    //    Rule::corn => "corn",
    //    Rule::monster => "monster",
    //    Rule::pizza => "pizza",
    //    Rule::twinkie => "twinkie",
    //    Rule::tide => "tide pod",
    //    Rule::chew => "chew",
    //    Rule::muffin => "muffin",
    //    _ => ""
    //}.to_owned()
    item

}
fn extract_item_inner<'a>(parsed: &mut pest::iterators::Pair<'a,Rule>) -> String {
    let item = parsed;
    match item.as_rule() {
        Rule::chimi => "chimi",
        Rule::burrito => "burrito",
        Rule::can => "can",
        Rule::bake => "bake",
        Rule::chips => "chips",
        Rule::corn => "corn",
        Rule::monster => "monster",
        Rule::pizza => "pizza",
        Rule::twinkie => "twinkie",
        Rule::tide => "tide pod",
        Rule::chew => "chew",
        Rule::muffin => "muffin",
        _ => ""
    }.to_owned()

}
fn extract_nick<'a>(parsed: &pest::iterators::Pair<'a,Rule>, requestor_nick: &String) -> String {
    let mut nick = String::new();

    for item in parsed.clone().into_inner() {
        match item.as_rule() {
            Rule::nick => {
                for inner_item in item.into_inner() {
                    nick.push_str( match inner_item.as_rule() {
                        Rule::nick_chars => inner_item.clone().into_span().as_str(),
                        _ => ""
                    });
                }
            },
            _ => ()
        }
    }

    if nick.is_empty() {
        nick = requestor_nick.to_string();
    } else {
        nick = nick.chars().rev().collect::<String>()
    }

    nick
}
